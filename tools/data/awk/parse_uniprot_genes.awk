# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Tab-separate the output.

BEGIN {
  FS = "   "
  OFS = "\t"
}

# Start of entry.

/^ID/ {
  identifier = $2
}

# Gene identifiers.

/^DR   GeneID/ {
  split($2, items, "; *")
  print identifier, items[2]
}
