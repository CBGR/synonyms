Synonyms
========

.. note:: Note that this repository is no longer required to generate synonym
   data for JASPAR and UniBind. Since the contents of the JASPAR database
   dictates which synonyms are generated, the activity has been moved to the
   `JASPAR data preparation software`_.

This software produces data containing gene synonyms from Entrez Gene's
``gene_info`` file that can be incorporated into the JASPAR and UniBind
databases.

Details of the workflow are provided in a separate `workflow`_ document. See
the `deployment`_ documentation for a convenient way of incorporating this
workflow into the deployment of other software.

.. _`JASPAR data preparation software`: https://bitbucket.org/CBGR/jp2021
.. _`deployment`: docs/Deployment.rst
.. _`workflow`: docs/Workflow.rst


Preparing Synonyms Data
-----------------------

A number of tools are involved in preparing synonyms data for other programs.
These are coordinated by a ``Makefile`` which may be used as follows:

.. code:: shell

  make

This should download required data to a working directory, process the data,
and produce output data for deployment in JASPAR and UniBind. This output data
can then be installed (deployed) as follows:

.. code:: shell

  make install

The installation location (by default ``/var/tmp/synonyms-data``) can be
indicated using a parameter. For example:

.. code:: shell

   make install OUTPUT_DATA=/tmp/synonyms-data

Other parameters can be specified include the location of the JASPAR database
file, the Entrez Gene information, and Swiss-Prot data, these being sources of
required data. For example:

.. code:: shell

  make JASPAR_DATA=/var/www/apps/jaspar2020/JASPAR2020.sqlite \
       GENE_INFO=/var/tmp/gene_info.gz \
       UNIPROT_SPROT=/var/tmp/uniprot_sprot.dat.gz

The default processing method uses an approach employing the SQLite database
system. The method can be changed using the ``METHOD`` parameter which can
have the following values:

- ``pgsql`` (use a PostgreSQL database)
- ``shell`` (use shell utilities)
- ``sqlite`` (use a SQLite database file)


Updating Databases
------------------

Deployment scripts are provided with UniBind and JASPAR for updating their
databases with generated synonyms data. These scripts invoke programs supplied
in this distribution: ``update_unibind`` and ``update_jaspar`` respectively.


Copyright and Licensing Information
-----------------------------------

Source code originating in this project is licensed under the terms of the GNU
General Public License version 3 or later (GPLv3 or later). Original content
is licensed under the Creative Commons Attribution Share Alike 4.0
International (CC-BY-SA 4.0) licensing terms.

See the ``.reuse/dep5`` file and the accompanying ``LICENSES`` directory in
this software's source code distribution for complete copyright and licensing
information, including the licensing details of bundled code and content.
