# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Download and process input data, producing synonym-related output data for
# deployment.

BASEDIR = $(CURDIR)
TOOLS = $(BASEDIR)/tools
DATATOOLS = $(TOOLS)/data
LINK = ln -s


# Data locations and sources.

WORK			?= $(BASEDIR)/work
OUTPUT_DATA		?= /var/tmp/synonyms-data

# Processing method.

METHOD			?= sqlite

# Database or processing area if using the shell approach.

ifeq '$(METHOD)' 'pgsql'
DATABASE		?= synonyms
else ifeq '$(METHOD)' 'sqlite'
DATABASE		?= $(WORK)/synonyms.sqlite
else
DATABASE		= $(WORK)
endif

# Remote sources.

GENE_INFO_URL		= ftp://ftp.ncbi.nih.gov/gene/DATA/gene_info.gz
UNIPROT_SPROT_URL	= ftp://ftp.ebi.ac.uk/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz

# Inputs.

GENE_INFO_CACHED	= $(WORK)/gene_info.gz
JASPAR_DATA_CACHED	= $(WORK)/JASPAR2020.sqlite
UNIPROT_SPROT_CACHED	= $(WORK)/uniprot_sprot.dat.gz

# Inputs from any previous processing.

GENE_INFO_OLD		= $(OUTPUT_DATA)/gene_info.gz
JASPAR_DATA_OLD		= $(OUTPUT_DATA)/JASPAR2020.sqlite
UNIPROT_SPROT_OLD	= $(OUTPUT_DATA)/uniprot_sprot.dat.gz

# Input products used to prepare the outputs.

PROFILES		= $(WORK)/profiles.jaspar
PROTEINS		= $(WORK)/proteins.jaspar
SYNONYMS_TABLE		= $(WORK)/synonyms.table
UNIPROT_ACCESSIONS	= $(WORK)/uniprot_accessions
UNIPROT_GENES		= $(WORK)/uniprot_genes

PRODUCTS = $(PROFILES) $(PROTEINS) $(SYNONYMS_TABLE) $(UNIPROT_ACCESSIONS) $(UNIPROT_GENES)

# Shell method and summary by-products.

UNIPROT_MAPPING		= $(WORK)/uniprot_mapping
SYNONYMS_SORTED		= $(WORK)/synonyms.sorted
SUMMARY_BYPRODUCTS	= $(WORK)/summary_work

BYPRODUCTS = $(UNIPROT_MAPPING) $(SYNONYMS_SORTED)

# Outputs for use by other software.

GENES			= $(WORK)/genes.jaspar
PROFILE_GENES		= $(WORK)/profile_genes.jaspar
SYNONYMS		= $(WORK)/synonyms.jaspar
SUMMARY			= $(WORK)/profile_summary.jaspar

# Outputs for installation including gene/protein source data.

OUTPUTS_PREPARED	= $(GENES) $(PROFILE_GENES) $(SYNONYMS)
OUTPUTS_FINAL		= $(OUTPUTS_PREPARED) $(PROFILES) $(SUMMARY)
OUTPUTS_ARCHIVES	= $(GENE_INFO_CACHED) $(UNIPROT_SPROT_CACHED)
OUTPUTS_NO_BACKUP	= $(JASPAR_DATA_CACHED)

# All outputs for deletion purposes.

ALL_OUTPUTS = $(OUTPUTS_PREPARED) $(OUTPUTS_PREPARED_MARKER) \
	      $(OUTPUTS_ARCHIVES) $(OUTPUTS_NO_BACKUP) \
	      $(PRODUCTS) $(BYPRODUCTS)

# Include the database for deletion if using SQLite.

ifeq '$(METHOD)' 'sqlite'
ALL_OUTPUTS += $(DATABASE)
endif

# Installed locations of the outputs.

OUTPUTS_INSTALLED = $(foreach OUTPUT,$(OUTPUTS_FINAL),$(OUTPUT_DATA)/$(shell basename $(OUTPUT)))



# Processing workflow.

.PHONY : all clean install

# Generate the outputs for subsequent installation by default.

all: $(OUTPUTS_FINAL)

# Clean all files involved as well as the work directory.

clean:
	-$(RM) $(ALL_OUTPUTS)
	-rmdir $(WORK)

# Install outputs for use by other software, also source data if still
# available.

install: $(OUTPUTS_INSTALLED)
	-$(TOOLS)/copy $(OUTPUTS_ARCHIVES) $(OUTPUT_DATA)
	-$(TOOLS)/copy --no-backup $(OUTPUTS_NO_BACKUP) $(OUTPUT_DATA)

# Define a rule for each output.

define install_template =
OUTPUT_INSTALLED_$(1) = $(OUTPUT_DATA)/$(shell basename $(1))

$$(OUTPUT_INSTALLED_$(1)): $(1) $(OUTPUT_DATA)
	$(TOOLS)/copy $(1) $(OUTPUT_DATA)
endef

$(foreach OUTPUT,$(OUTPUTS_FINAL),\
$(eval $(call install_template,$(OUTPUT))))



# Make sure that the output directory exists.

$(OUTPUT_DATA):
	mkdir -p $(OUTPUT_DATA)

# Make sure that the work directory exists.

$(ALL_OUTPUTS): | $(WORK)

$(WORK):
	mkdir -p $(WORK)



# Download data or use existing downloads. Existing downloads will be referenced
# using symbolic links and will therefore not be reinstalled.

$(GENE_INFO_CACHED):
	if [ -e "$(GENE_INFO)" ] ; then \
		$(LINK) `realpath $(GENE_INFO)` $(GENE_INFO_CACHED) ; \
	elif [ -e $(GENE_INFO_OLD) ] ; then \
		$(LINK) `realpath $(GENE_INFO_OLD)` $(GENE_INFO_CACHED) ; \
	else wget -nv -P $(WORK) $(GENE_INFO_URL) ; \
	fi

$(UNIPROT_SPROT_CACHED):
	if [ -e "$(UNIPROT_SPROT)" ] ; then \
		$(LINK) `realpath $(UNIPROT_SPROT)` $(UNIPROT_SPROT_CACHED) ; \
	elif [ -e $(UNIPROT_SPROT_OLD) ] ; then \
		$(LINK) `realpath $(UNIPROT_SPROT_OLD)` $(UNIPROT_SPROT_CACHED) ; \
	else wget -nv -P $(WORK) $(UNIPROT_SPROT_URL) ; \
	fi

$(JASPAR_DATA_CACHED):
	if [ -e "$(JASPAR_DATA)" ] ; then \
		$(LINK) `realpath $(JASPAR_DATA)` $(JASPAR_DATA_CACHED) ; \
	elif [ -e $(JASPAR_DATA_OLD) ] ; then \
		$(LINK) `realpath $(JASPAR_DATA_OLD)` $(JASPAR_DATA_CACHED) ; \
	else $(DATATOOLS)/fetch_jaspar_data $(WORK) ; \
	fi



# Prepare a synonyms table from the gene_info data.

$(SYNONYMS_TABLE): $(GENE_INFO_CACHED)
	gunzip -c $(GENE_INFO_CACHED) | $(DATATOOLS)/make_synonyms > $(SYNONYMS_TABLE)

# Prepare UniProt details.

$(UNIPROT_ACCESSIONS): $(UNIPROT_SPROT_CACHED)
	gunzip -c $(UNIPROT_SPROT_CACHED) | $(DATATOOLS)/parse_uniprot_accessions > $(UNIPROT_ACCESSIONS)

$(UNIPROT_GENES): $(UNIPROT_SPROT_CACHED)
	gunzip -c $(UNIPROT_SPROT_CACHED) | $(DATATOOLS)/parse_uniprot_genes > $(UNIPROT_GENES)

# Extract JASPAR data.

$(PROFILES): $(JASPAR_DATA_CACHED)
	$(DATATOOLS)/jaspar_profiles $(JASPAR_DATA_CACHED) > $(PROFILES)

$(PROTEINS): $(JASPAR_DATA_CACHED)
	$(DATATOOLS)/jaspar_proteins $(JASPAR_DATA_CACHED) > $(PROTEINS)



# Prepare and export data.

# Use the indicated method to prepare the database required for the eventually
# exported data. Then, export the results from the database if one is involved.

include $(CURDIR)/mk/grouped.mk

OUTPUTS_PREPARED_MARKER = $(WORK)/_prepared_

$(eval $(call grouped_target,$(OUTPUTS_PREPARED),$(OUTPUTS_PREPARED_MARKER)))

$(OUTPUTS_PREPARED_MARKER): $(PRODUCTS)
	$(TOOLS)/$(METHOD)/prepare_synonyms $(DATABASE) $(WORK)
	if [ $(METHOD) != 'shell' ] ; then $(TOOLS)/$(METHOD)/export_prepared_synonyms $(DATABASE) $(WORK) ; fi
	touch $@



# Prepare a more convenient summary of synonyms for profiles.
# Remove the byproducts in this rule after processing.

$(SUMMARY): $(OUTPUTS_PREPARED) $(PROFILES)
	$(DATATOOLS)/profile_summary $(WORK) $(SUMMARY_BYPRODUCTS) > $(SUMMARY)
	-rm $(SUMMARY_BYPRODUCTS)/*
	-rmdir $(SUMMARY_BYPRODUCTS)
