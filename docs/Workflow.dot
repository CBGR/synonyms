/* dot -Tsvg -o Workflow.svg Workflow.dot */

digraph workflow {
    graph [splines=ortho];
    node [shape=record,style=filled,fillcolor=lightgray,fontname="Helvetica",fontsize=12];
    edge [fontname="Helvetica",fontsize=10];

    /* Data sources. */

    subgraph {
        rank=same;
        node [shape=cylinder,fillcolor=lightcyan];

        gene_info               [label="Entrez Gene\ngene_info"];
        jaspar_data             [label="JASPAR\ndatabase.tar.gz"];
        uniprot_sprot           [label="UniProt\nuniprot_sprot"];
    }

    /* Intermediate files/tables. */

    gene_synonyms           [label="{ gene_synonyms | { tax_id | gene_id | gene_symbol | gene_synonym } }"];
    jaspar_profiles         [label="{ jaspar_profiles | { profile_id | jaspar_id | jaspar_version | name } }"];
    jaspar_proteins         [label="{ jaspar_proteins | { profile_id | uniprot_ac } }"];
    profile_gene_info       [label="{ profile_gene_info | { gene_id | tax_id | gene_symbol } }"];
    profile_gene_synonyms   [label="{ profile_gene_synonyms | { gene_id | gene_synonym } }"];
    profile_genes           [label="{ profile_genes | { profile_id | gene_id } }"];

    uniprot_accessions      [label="{ uniprot_accessions | { uniprot_id | uniprot_ac } }"];
    uniprot_genes           [label="{ uniprot_genes | { uniprot_id | gene_id } }"];
    uniprot_mapping         [label="{ uniprot_mapping | { uniprot_ac | gene_id } }"];

    /* Outputs. */

    subgraph {
        rank=same;
        node [shape=box,fillcolor=lightcyan];

        genes_jaspar            [label="Gene details\n(genes.jaspar)"];
        synonyms_jaspar         [label="Profile synonyms\n(synonyms.jaspar)"];
        profile_genes_jaspar    [label="Profile-gene mapping\n(profile_genes.jaspar)"];
        profiles_jaspar         [label="Profiles\n(profiles.jaspar)"];
        profile_summary_jaspar  [label="Profile synonyms\n(profile_summary.jaspar)"];
    }

    /* Processing operations. */

    map_accessions_to_genes     [shape=ellipse,fillcolor=white,label="map accessions to genes"];
    map_profiles_to_genes       [shape=ellipse,fillcolor=white,label="map profiles to genes"];
    map_profiles_to_synonyms    [shape=ellipse,fillcolor=white,label="map profiles to synonyms"];
    filter_genes_for_profiles   [shape=ellipse,fillcolor=white,label="filter genes for profiles"];
    profile_summary             [shape=ellipse,fillcolor=white,label="make synonyms summary"];

    /* Process inputs. */

    gene_info -> gene_synonyms;
    uniprot_sprot -> uniprot_accessions;
    uniprot_sprot -> uniprot_genes;
    jaspar_data -> jaspar_profiles;
    jaspar_data -> jaspar_proteins;

    uniprot_accessions -> map_accessions_to_genes;
    uniprot_genes -> map_accessions_to_genes;
    map_accessions_to_genes -> uniprot_mapping;

    jaspar_proteins -> map_profiles_to_genes;
    uniprot_mapping -> map_profiles_to_genes;
    map_profiles_to_genes -> profile_genes;

    gene_synonyms -> filter_genes_for_profiles;
    profile_genes -> filter_genes_for_profiles;
    filter_genes_for_profiles -> profile_gene_info;

    gene_synonyms -> map_profiles_to_synonyms;
    profile_genes -> map_profiles_to_synonyms;
    map_profiles_to_synonyms -> profile_gene_synonyms;

    profile_gene_info -> genes_jaspar;
    profile_gene_synonyms -> synonyms_jaspar;
    profile_genes -> profile_genes_jaspar;
    jaspar_profiles -> profiles_jaspar;

    /* Generate profile synonyms summary. */

    profiles_jaspar -> profile_summary;
    profile_genes_jaspar -> profile_summary;
    synonyms_jaspar -> profile_summary;
    profile_summary -> profile_summary_jaspar;
}
