Shell Script Approach
=====================

A shell-script-driven approach to generating synonym-related data can be taken
as follows:

.. code:: shell

  tools/shell/prepare_synonyms DATADIR

The indicated ``DATADIR`` must contain the necessary input files.

This should produce the following files:

- ``genes.jaspar``
- ``synonyms.jaspar``
- ``profile_genes.jaspar``

The procedure is described below.

Procedure Details
-----------------

First, UniProt accessions and gene identifiers are first combined to create an
accession-to-gene mapping:

.. code:: shell

  tools/shell/make_uniprot_mapping uniprot_accessions uniprot_genes > uniprot_mapping

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       UniProt accession
2       Gene identifier
======= ===================================================================

The complete synonyms collection must be sorted appropriately for combination
with selected genes:

.. code:: shell

  tools/shell/sort_synonyms < synonyms.table > synonyms.sorted

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       Taxonomy identifier
2       Gene identifier
3       Gene symbol
4       Synonym
======= ===================================================================

With the above files, it is now possible to attempt to produce the necessary
data for database tables.

Profile Gene Information
''''''''''''''''''''''''

To map the surrogate key values (record numbers) to gene identifiers, the
following commands can be run:

.. code:: shell

  tools/shell/make_profile_genes proteins.jaspar uniprot_mapping > profile_genes.jaspar

This will ultimately produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       Record number
2       Profile record number (from above)
3       Gene identifier
======= ===================================================================

Gene Information
''''''''''''''''

Basic gene information can be selected from the synonym information,
restricting it to the genes referenced by JASPAR proteins:

.. code:: shell

  tools/shell/select_genes synonyms.sorted profile_genes.jaspar > genes.jaspar

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       Gene identifier
2       Taxonomy identifier
3       Gene symbol
======= ===================================================================

Synonym Information
'''''''''''''''''''

Synonym information can be similarly selected:

.. code:: shell

  tools/shell/select_synonyms synonyms.sorted profile_genes.jaspar > synonyms.jaspar

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       Record number
2       Gene identifier
3       Synonym
======= ===================================================================
