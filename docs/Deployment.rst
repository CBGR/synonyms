Deployment
==========

A suite of deployment scripts has been provided to automate the deployment of
this software and the data it generates. The intention is to use these scripts
when deploying the data in conjunction with other applications.

The suite is run using the ``deploy.sh`` script which will then invoke the
different scripts found in the ``deploy`` directory in turn to attempt to
prepare the software environment and the generated data.

Where scripts are marked as ``root`` they will need to be run with elevated
privileges. To see what the ``deploy.sh`` script will do with regard to script
invocation, see the output of running it with the ``-n`` option:

.. code:: shell

  ./deploy.sh -n

To perform the necessary installation and configuration activities, and to
generate the data, the following command can be run:

.. code:: shell

  ./deploy.sh

The outcome should be a collection of data files in the
``/var/tmp/synonyms-data`` directory.
