Format Investigation
====================

The pertinent columns of the ``gene_info`` file are...

======= ===================================================================
Number  Description
======= ===================================================================
1       Taxonomy identifier
2       Gene identifier
3       Gene symbol
5       Gene synonyms (pipe-separated)
6       Database identifiers (pipe-separated)
11      Authoritative symbol
12      Authoritative name
14      Other names (pipe-separated)
======= ===================================================================

To distill the file and produce essential information, the following command
can be used to obtain the basic gene and name information:

.. code:: shell

  gunzip -c gene_info.gz | cut -f 1,2,3,5 > synonyms.txt

Since column #5 has an internal structure that requires further
interpretation, the generated file is not particularly convenient for more
sophisticated searching. Columns #6 and #14 also need further interpretation
to access individual values.

Examples
--------

The synonyms for the gene LFY_ can be obtained as follows:

.. code:: shell

  grep $'^3702\t' synonyms.txt | grep LFY | cut -f 4 | sed 's/|/\n/g'

.. _LFY: https://www.ncbi.nlm.nih.gov/gene/?term=836307

The following results are obtained:

::

  LEAFY
  LEAFY 3
  LFY3
  MAC9.13
  MAC9_13

This query could have matched names and synonyms containing ``LFY``. Here, the
organism was indicated to restrict the results appropriately.


Unpacking the Synonym Collections
---------------------------------

To access individual synonyms in the data, a script is provided to unpack them
into separate records. This script can be run as follows:

.. code:: shell

  gunzip -c gene_info.gz | tools/make_synonyms > synonyms.table

The ``make_synonyms`` tool produces tabular output with one synonym or name
per record, replicating the core fields of each original record for each
single synonym record.

======= ===================================================================
Number  Description
======= ===================================================================
1       Taxonomy identifier
2       Gene identifier
3       Symbol
4       Synonym
======= ===================================================================


Simple Processing and Querying Tools
------------------------------------

Some tools have been written to produce usable data for further inspection.
These tools require PostgreSQL whose packages on a Red Hat distribution are as
follows:

======================= ===================================================
Package                 Purpose
======================= ===================================================
``postgresql``          Client tools such as ``psql``
``postgresql-server``   Database server
``libpq-devel``         Client libraries (required for Python/Django)
======================= ===================================================

To manage the data in a database system, some SQL scripts have been provided
in the ``sql`` directory. These are wrapped by tools for initialisation and
querying.

To initialise a database table with the ``synonyms.table`` file generated
above in the given ``DATABASE``...

.. code:: shell

  tools/simple/init_synonyms synonyms.table DATABASE

To query the table...

.. code:: shell

  tools/simple/query_synonyms 'p53 & factor' DATABASE

Here, two terms - ``p53`` and ``factor`` - are both required in any synonym to
be matched.

Examples
++++++++

Searching using ``LFY`` to see if the gene LFY_ appears in the results:

.. code:: shell

  tools/simple/query_synonyms 'LFY' DATABASE

This will yield the following relevant result amongst other results:

::

  3702 |    836307 | LFY          | floral meristem identity control protein LEAFY (LFY)

Searching using ``LEAFY`` to see if LFY_ still appears, as well as other
matching entries:

.. code:: shell

  tools/simple/query_synonyms 'LEAFY' DATABASE

This will yield the following relevant results amongst many other results:

::

  3702 |    836307 | LFY          | floral meristem identity control protein LEAFY (LFY)
  3702 |    836307 | LFY          | LEAFY
  3702 |    836307 | LFY          | LEAFY 3
