Workflow
========

This document describes the retrieval of gene synonyms from Entrez Gene's
``gene_info`` file and their incorporation into the JASPAR and UniBind
databases. Details of the file format and other investigations are provided in
a separate `format`_ document.

.. _`format`: Format.rst


.. contents::


Overview
--------

An overview of the workflow is shown below:

.. image:: Workflow.svg

To summarise, the following steps are taken to process the data:

#. Obtain and unpack input data.

#. Combine UniProt mappings to produce an accession-to-gene mapping.

#. Combine this accession-to-gene mapping with JASPAR profile-to-protein
   mapping, thus obtaining a profile-to-gene mapping.

#. Combine gene synonym details to obtain a profile-to-synonym mapping.


Obtaining Input Data
--------------------

The ``gene_info`` file is downloaded as follows:

.. code:: shell

  wget ftp://ftp.ncbi.nih.gov/gene/DATA/gene_info.gz

Protein accession details are also required from SwissProt/UniProt and are
downloaded as follows:

.. code:: shell

  wget ftp://ftp.ebi.ac.uk/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz


Unpacking the Synonym Collections
---------------------------------

To access individual synonyms in the data, a script is provided to unpack them
into separate records. This script can be run as follows:

.. code:: shell

  gunzip -c gene_info.gz | tools/data/make_synonyms > synonyms.table

The ``make_synonyms`` tool produces tabular output with one synonym or name
per record, replicating the core fields of each original record for each
single synonym record.

======= ===================================================================
Number  Description
======= ===================================================================
1       Taxonomy identifier
2       Gene identifier
3       Symbol
4       Synonym
======= ===================================================================


Selecting Identifiers for UniBind and JASPAR
--------------------------------------------

The name information used by UniBind and JASPAR is not sufficient to
unambiguously identify genes. Consequently, protein information has to be
extracted from JASPAR:

.. code:: shell

  tools/data/jaspar_proteins ../jaspar2020/download/database.tar.gz > proteins.jaspar

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       Record number
2       UniProt accessions
======= ===================================================================

Profile information from JASPAR is also required for eventual import into
UniBind and is extracted as follows from the JASPAR database archive:

.. code:: shell

  tools/data/jaspar_profiles ../jaspar2020/download/database.tar.gz > profiles.jaspar

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       Record number
2       JASPAR base identifier
3       JASPAR version
======= ===================================================================

The UniProt accessions need to be found in the UniProt database and their
details are extracted as follows:

.. code:: shell

  gunzip -c uniprot_sprot.dat.gz | tools/data/parse_uniprot_accessions > uniprot_accessions

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       UniProt identifier
2       UniProt accession
======= ===================================================================

Gene identifiers are also extracted:

.. code:: shell

  gunzip -c uniprot_sprot.dat.gz | tools/data/parse_uniprot_genes > uniprot_genes

This will produce a file of the following form:

======= ===================================================================
Column  Description
======= ===================================================================
1       UniProt identifier
2       Gene identifier
======= ===================================================================

Database Approach
+++++++++++++++++

Given an appropriate database name (indicated as ``DATABASE`` below) and a
directory containing input data (indicated as ``DATADIR`` below), a
database-driven approach can be used to generate synonym-related data.

For PostgreSQL, the following commands are used:

.. code:: shell

  tools/pgsql/prepare_synonyms DATABASE DATADIR
  tools/pgsql/export_prepared_synonyms DATABASE DATADIR

For SQLite, the following are used instead:

.. code:: shell

  tools/sqlite/prepare_synonyms DATABASE DATADIR
  tools/sqlite/export_prepared_synonyms DATABASE DATADIR

This should produce the following files:

- ``genes.jaspar``
- ``synonyms.jaspar``
- ``profile_genes.jaspar``

Shell Script Approach
+++++++++++++++++++++

A shell-script-driven approach is described `separately`_.

.. _`separately`: Shell.rst

Model Summary
+++++++++++++

The above approaches can be used for populating a separate database. In JASPAR
and UniBind themselves, the numbering of profile instances and the population
of a suitable table can be done using an appropriate query or by writing code
to use the Django object-relational mapper.

The correspondence between generated files and Django models is as follows:

================= =========================================================
Model             File
================= =========================================================
``Gene``          ``genes.jaspar``
``Synonym``       ``synonyms.jaspar``
``ProfileGene``   ``profile_genes.jaspar``
``Profiles``      ``profiles.jaspar``
================= =========================================================
