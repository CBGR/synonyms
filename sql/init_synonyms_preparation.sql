-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Gene synonyms prepared directly from gene_info.

create table gene_synonyms (
    tax_id integer not null,
    gene_id integer not null,
    gene_symbol varchar not null,
    gene_synonym varchar not null
);

-- Protein records from JASPAR.

create table jaspar_proteins (
    profile_id integer not null,
    uniprot_ac varchar not null
);

-- UniProt identifier-to-accession mapping.

create table uniprot_accessions (
    uniprot_id varchar not null,
    uniprot_ac varchar not null
);

-- UniProt identifier-to-gene mapping.

create table uniprot_genes (
    uniprot_id varchar not null,
    gene_id integer not null
);

-- Computed UniProt accession-to-gene mapping.

create table uniprot_mapping (
    uniprot_ac varchar not null,
    gene_id integer not null
);

-- Computed JASPAR profile-to-gene mapping (involving surrogate keys).

create table profile_genes (
    profile_id integer not null,
    gene_id integer not null
);

-- Computed concise gene information for profile genes only.

create table profile_gene_info (
    gene_id integer not null,
    tax_id integer not null,
    gene_symbol varchar not null
);

-- Computed concise gene-to-synonym mapping for profile genes only.

create table profile_gene_synonyms (
    gene_id integer not null,
    gene_synonym varchar not null
);
