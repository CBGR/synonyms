-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

drop table gene_synonyms;
drop table jaspar_proteins;
drop table uniprot_accessions;
drop table uniprot_genes;
drop table uniprot_mapping;
drop table profile_genes;
drop table profile_gene_info;
drop table profile_gene_synonyms;
