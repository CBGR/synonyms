#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
TOOLSDIR="$BASEDIR/tools"

# Define a common storage location.

OUTPUT_DATA="/var/tmp/synonyms-data"

# Define the expected files to be deployed.

FILENAMES="genes.jaspar profile_genes.jaspar profiles.jaspar synonyms.jaspar"

# Avoid problems with limited /tmp partition allocations.

export TMPDIR="$OUTPUT_DATA"

if [ ! -e "$TMPDIR" ] ; then
    mkdir -p "$TMPDIR"
fi

# Download, prepare and export the data products if they are absent.

for FILENAME in $FILENAMES ; do
    if [ ! -e "$OUTPUT_DATA/$FILENAME" ] ; then
        make -C "$BASEDIR" OUTPUT_DATA="$OUTPUT_DATA" install
        break
    fi
done
