#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Test for dependencies.

INSTALLED=`xargs dnf -q info --installed < "$BASEDIR/requirements-sys.txt" | grep ^Name | wc -l`
REQUIRED=`wc -l < "$BASEDIR/requirements-sys.txt"`

if [ "$INSTALLED" = "$REQUIRED" ] ; then
    exit 0
fi

# Install system dependencies.

xargs dnf -y install < "$BASEDIR/requirements-sys.txt"

# vim: tabstop=4 expandtab shiftwidth=4
