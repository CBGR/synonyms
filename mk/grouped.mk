# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

define grouped_target =
$(1): $(2)

$(foreach FILE,$(1),$(if $(wildcard $(FILE)),,$(shell rm -f $(2))))
endef
